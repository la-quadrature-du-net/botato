import asyncio
import os
import yaml
from mastodon import Mastodon
from nio import AsyncClient, MatrixRoom, RoomMessageText


class MastodonMatrixBot:
    def __init__(self, config_file="config/config.yml"):
        self.config = self.load_config(config_file)

        self.mastodon = Mastodon(
            access_token=self.config["mastodon"]["token"],
            api_base_url=self.config["mastodon"]["instance"],
        )

        self.matrix_client = AsyncClient(self.config["matrix"]["homeserver"], self.config["matrix"]["matrix_user_id"] )
        self.matrix_client.user_id = self.config["matrix"]["user_id"]
        self.matrix_client.access_token = self.config["matrix"]["bot_token"]

    def load_config(self, config_file):
        if os.path.exists(config_file):
            with open(config_file, "r") as file:
                return yaml.safe_load(file)
        else:
            raise FileNotFoundError(f"Config file not found: {config_file}")



    async def start(self):
        device_name = self.config["matrix"]["device_name"]

        await self.matrix_client.login(
            token=self.config["matrix"]["bot_token"],
            device_name=device_name,
            user_id=matrix_user_id,
        )

        while True:
            try:
                await self.matrix_client.sync(timeout=30000, full_state=True)
                # Your custom logic here, e.g., handling events
            except Exception as e:
                print(f"Error during sync: {e}")
                # Handle the exception, maybe wait for a while before retrying
                await asyncio.sleep(10)

    async def matrix_message_handler(self, room: MatrixRoom, event: RoomMessageText):
        if event.sender != self.matrix_client.user:
            # Process the Matrix message
            command_prefix = "!"
            if event.body.startswith(command_prefix):
                # Extract the command after the prefix
                command = event.body[len(command_prefix) :].strip()

                # Execute the command on Mastodon
                mastodon_response = self.execute_mastodon_command(command)

                # Send the Mastodon response back to the Matrix room
                await self.matrix_client.room_send(
                    room_id=room.room_id,
                    message_type="m.room.message",
                    content={"msgtype": "m.text", "body": mastodon_response},
                )

    def execute_mastodon_command(self, command):
        # Split the command into parts
        command_parts = command.split()

        # Check the first part of the command to determine the action
        action = command_parts[0].lower()

        if action == "toot":
            # Extract the toot text from the command
            toot_text = " ".join(command_parts[1:])

            # Post a toot with text
            # media_ids = self.upload_images(["path/to/image1.jpg", "path/to/image2.png"])
            # toot_response = self.post_toot(toot_text, media_ids)
            toot_response = self.post_toot(toot_text)

            return f"Tooted: {toot_response['url']}"

        elif action == "boost":
            # Extract the URL of the post to boost
            post_url = command_parts[1]

            # Boost a post from URL
            boost_response = self.boost_post(post_url)

            return f"Boosted post: {boost_response['url']}"

        else:
            return "Invalid command. Supported actions: toot, boost"

    def upload_images(self, image_paths):
        media_ids = []

        for image_path in image_paths:
            with open(image_path, "rb") as image_file:
                response = self.mastodon.media_post(media_file=image_file)
                media_ids.append(response["id"])

        return media_ids

    def post_toot(self, toot_text, media_ids):
        status = self.mastodon.status_post(status=toot_text, media_ids=media_ids)

        return status

    def boost_post(self, post_url):
        # Extract the status ID from the Mastodon status URL
        status_id = int(post_url.split("/")[-1])

        # Boost the post using the status ID
        boost_response = self.mastodon.status_reblog(status_id)

        return boost_response


if __name__ == "__main__":
    bot = MastodonMatrixBot()

    loop = asyncio.get_event_loop()

    async def main():
        await bot.start()

    loop.run_until_complete(main())